class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.string :first_name
      t.string :last_name
      t.binary :profile_image
      t.string :mobile
      t.string :email
      t.string :status

      t.timestamps
    end
  end
end
