class CreateCollectEducationDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :collect_education_details do |t|
      t.string :degree
      t.string :institute
      t.string :university
      t.string :year_of_passing
      t.string :percentage_CGPA
      t.string :edu_file_attachment

      t.timestamps
    end
  end
end
