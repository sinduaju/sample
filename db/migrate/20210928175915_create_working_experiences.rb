class CreateWorkingExperiences < ActiveRecord::Migration[5.2]
  def change
    create_table :working_experiences do |t|
      t.string :company_name
      t.string :designation
      t.binary :from_date
      t.string :to_date
      t.string :duration
      t.string :exp_file_upload

      t.timestamps
    end
  end
end
