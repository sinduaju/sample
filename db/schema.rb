# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_09_28_175915) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "collect_education_details", force: :cascade do |t|
    t.string "degree"
    t.string "institute"
    t.string "university"
    t.string "year_of_passing"
    t.string "percentage_CGPA"
    t.string "edu_file_attachment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "education_details", force: :cascade do |t|
    t.string "degree"
    t.string "institute"
    t.string "university"
    t.string "year_of_passing"
    t.string "percentage_CGPA"
    t.string "edu_file_attachment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "profiles", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.binary "profile_image"
    t.string "mobile"
    t.string "email"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "role"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "working_experiences", force: :cascade do |t|
    t.string "company_name"
    t.string "designation"
    t.binary "from_date"
    t.string "to_date"
    t.string "duration"
    t.string "exp_file_upload"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
