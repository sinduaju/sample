require "application_system_test_case"

class EducationDetailsTest < ApplicationSystemTestCase
  setup do
    @education_detail = education_details(:one)
  end

  test "visiting the index" do
    visit education_details_url
    assert_selector "h1", text: "Education Details"
  end

  test "creating a Education detail" do
    visit education_details_url
    click_on "New Education Detail"

    fill_in "Degree", with: @education_detail.degree
    fill_in "Edu file attachment", with: @education_detail.edu_file_attachment
    fill_in "Institute", with: @education_detail.institute
    fill_in "Percentage cgpa", with: @education_detail.percentage_CGPA
    fill_in "University", with: @education_detail.university
    fill_in "Year of passing", with: @education_detail.year_of_passing
    click_on "Create Education detail"

    assert_text "Education detail was successfully created"
    click_on "Back"
  end

  test "updating a Education detail" do
    visit education_details_url
    click_on "Edit", match: :first

    fill_in "Degree", with: @education_detail.degree
    fill_in "Edu file attachment", with: @education_detail.edu_file_attachment
    fill_in "Institute", with: @education_detail.institute
    fill_in "Percentage cgpa", with: @education_detail.percentage_CGPA
    fill_in "University", with: @education_detail.university
    fill_in "Year of passing", with: @education_detail.year_of_passing
    click_on "Update Education detail"

    assert_text "Education detail was successfully updated"
    click_on "Back"
  end

  test "destroying a Education detail" do
    visit education_details_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Education detail was successfully destroyed"
  end
end
