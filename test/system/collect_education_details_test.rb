require "application_system_test_case"

class CollectEducationDetailsTest < ApplicationSystemTestCase
  setup do
    @collect_education_detail = collect_education_details(:one)
  end

  test "visiting the index" do
    visit collect_education_details_url
    assert_selector "h1", text: "Collect Education Details"
  end

  test "creating a Collect education detail" do
    visit collect_education_details_url
    click_on "New Collect Education Detail"

    fill_in "Degree", with: @collect_education_detail.degree
    fill_in "Edu file attachment", with: @collect_education_detail.edu_file_attachment
    fill_in "Institute", with: @collect_education_detail.institute
    fill_in "Percentage cgpa", with: @collect_education_detail.percentage_CGPA
    fill_in "University", with: @collect_education_detail.university
    fill_in "Year of passing", with: @collect_education_detail.year_of_passing
    click_on "Create Collect education detail"

    assert_text "Collect education detail was successfully created"
    click_on "Back"
  end

  test "updating a Collect education detail" do
    visit collect_education_details_url
    click_on "Edit", match: :first

    fill_in "Degree", with: @collect_education_detail.degree
    fill_in "Edu file attachment", with: @collect_education_detail.edu_file_attachment
    fill_in "Institute", with: @collect_education_detail.institute
    fill_in "Percentage cgpa", with: @collect_education_detail.percentage_CGPA
    fill_in "University", with: @collect_education_detail.university
    fill_in "Year of passing", with: @collect_education_detail.year_of_passing
    click_on "Update Collect education detail"

    assert_text "Collect education detail was successfully updated"
    click_on "Back"
  end

  test "destroying a Collect education detail" do
    visit collect_education_details_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Collect education detail was successfully destroyed"
  end
end
