require 'test_helper'

class CollectEducationDetailsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @collect_education_detail = collect_education_details(:one)
  end

  test "should get index" do
    get collect_education_details_url
    assert_response :success
  end

  test "should get new" do
    get new_collect_education_detail_url
    assert_response :success
  end

  test "should create collect_education_detail" do
    assert_difference('CollectEducationDetail.count') do
      post collect_education_details_url, params: { collect_education_detail: { degree: @collect_education_detail.degree, edu_file_attachment: @collect_education_detail.edu_file_attachment, institute: @collect_education_detail.institute, percentage_CGPA: @collect_education_detail.percentage_CGPA, university: @collect_education_detail.university, year_of_passing: @collect_education_detail.year_of_passing } }
    end

    assert_redirected_to collect_education_detail_url(CollectEducationDetail.last)
  end

  test "should show collect_education_detail" do
    get collect_education_detail_url(@collect_education_detail)
    assert_response :success
  end

  test "should get edit" do
    get edit_collect_education_detail_url(@collect_education_detail)
    assert_response :success
  end

  test "should update collect_education_detail" do
    patch collect_education_detail_url(@collect_education_detail), params: { collect_education_detail: { degree: @collect_education_detail.degree, edu_file_attachment: @collect_education_detail.edu_file_attachment, institute: @collect_education_detail.institute, percentage_CGPA: @collect_education_detail.percentage_CGPA, university: @collect_education_detail.university, year_of_passing: @collect_education_detail.year_of_passing } }
    assert_redirected_to collect_education_detail_url(@collect_education_detail)
  end

  test "should destroy collect_education_detail" do
    assert_difference('CollectEducationDetail.count', -1) do
      delete collect_education_detail_url(@collect_education_detail)
    end

    assert_redirected_to collect_education_details_url
  end
end
