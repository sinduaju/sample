class EducationDetail < ApplicationRecord
  mount_uploader :edu_file_attachment, AttachmentUploader # Tells rails to use this uploader for this model.   
  validates :degree, presence: true # Make sure the owner's name is present. 
end
