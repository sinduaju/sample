class WorkingExperience < ApplicationRecord
  mount_uploader :exp_file_upload, AttachmentUploader # Tells rails to use this uploader for this model.   
  validates :company_name, presence: true # Make sure the owner's name is present. 
end
