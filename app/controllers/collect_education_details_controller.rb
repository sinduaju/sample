class CollectEducationDetailsController < ApplicationController
  before_action :set_collect_education_detail, only: %i[ show edit update destroy ]

  # GET /collect_education_details or /collect_education_details.json
  def index
    @collect_education_details = CollectEducationDetail.all
  end

  # GET /collect_education_details/1 or /collect_education_details/1.json
  def show
  end

  # GET /collect_education_details/new
  def new
    @collect_education_detail = CollectEducationDetail.new
  end

  # GET /collect_education_details/1/edit
  def edit
  end

  # POST /collect_education_details or /collect_education_details.json
  def create
    @collect_education_detail = CollectEducationDetail.new(collect_education_detail_params)

    respond_to do |format|
      if @collect_education_detail.save
        format.html { redirect_to @collect_education_detail, notice: "Collect education detail was successfully created." }
        format.json { render :show, status: :created, location: @collect_education_detail }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @collect_education_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /collect_education_details/1 or /collect_education_details/1.json
  def update
    respond_to do |format|
      if @collect_education_detail.update(collect_education_detail_params)
        format.html { redirect_to @collect_education_detail, notice: "Collect education detail was successfully updated." }
        format.json { render :show, status: :ok, location: @collect_education_detail }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @collect_education_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /collect_education_details/1 or /collect_education_details/1.json
  def destroy
    @collect_education_detail.destroy
    respond_to do |format|
      format.html { redirect_to collect_education_details_url, notice: "Collect education detail was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_collect_education_detail
      @collect_education_detail = CollectEducationDetail.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def collect_education_detail_params
      params.require(:collect_education_detail).permit(:degree, :institute, :university, :year_of_passing, :percentage_CGPA, :edu_file_attachment)
    end
end
