json.extract! working_experience, :id, :company_name, :designation, :from_date, :to_date, :duration, :exp_file_upload, :created_at, :updated_at
json.url working_experience_url(working_experience, format: :json)
