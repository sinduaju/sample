json.extract! collect_education_detail, :id, :degree, :institute, :university, :year_of_passing, :percentage_CGPA, :edu_file_attachment, :created_at, :updated_at
json.url collect_education_detail_url(collect_education_detail, format: :json)
