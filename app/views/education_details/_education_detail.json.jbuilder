json.extract! education_detail, :id, :degree, :institute, :university, :year_of_passing, :percentage_CGPA, :edu_file_attachment, :created_at, :updated_at
json.url education_detail_url(education_detail, format: :json)
