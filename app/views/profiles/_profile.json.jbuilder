json.extract! profile, :id, :first_name, :last_name, :profile_image, :mobile, :email, :status, :created_at, :updated_at
json.url profile_url(profile, format: :json)
