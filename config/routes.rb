Rails.application.routes.draw do
  

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :profiles
  resources :working_experiences
  resources :collect_education_details
  resources :education_details
  
end
